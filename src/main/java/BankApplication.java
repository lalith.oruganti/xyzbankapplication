import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class BankApplication {
    static private int accountId;
    public static void main(String args[]) {
        Balance balance= new Balance();
        CustomerController control = new CustomerController();
        CustomerDetails cu = new CustomerDetails();
        CustomerAddress address = new CustomerAddress();
        control.postUser(cu);
        Scanner sc = new Scanner(System.in);
        System.out.println("XYZ bank \n 1.savings account \n 2.current account");
        List<CustomerDetails> users = new ArrayList<>();
        int ch = sc.nextInt();
        if(ch==1)
            cu.setType("Savings Account");
        else
            cu.setType("Current Account");
        System.out.println("Enter your name");
        String name = sc.next();
        cu.setName(name);
        System.out.println("Enter the Address in city,state,pin format\nEnter City");
        String city = sc.next();
        System.out.println("Enter state");
        String state = sc.next();
        System.out.println("Enter pin");
        int pin = sc.nextInt();
        address.setPin(pin);
        cu.setBalance(0.0);
        cu.setCreatedDate(java.time.LocalTime.now());
        cu.setStatus("Active");
        address.setState(state);
        address.setCity(city);
        users.add(cu);
        cu.setAccountId(++accountId);

        System.out.println("Enter amount to be added");
        double amount =sc.nextDouble();
        balance.deposit(amount,cu);
        System.out.println("Enter amount to be withdrawn");
        double amount1 =sc.nextDouble();
        balance.withdraw(amount1, cu);
        System.out.println("Balance"+cu.getBalance());
        System.out.println("User Details"+users);
    }
}