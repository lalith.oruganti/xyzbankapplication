public class Balance {

    public void deposit(double amount,CustomerDetails cu){
        double balance = cu.getBalance();
        cu.setBalance(balance+amount);

    }
    public  void withdraw(double amount, CustomerDetails cu){
        double balance = cu.getBalance();
        if(balance<amount)
            System.out.println("Balance not sufficient");
        else
            cu.setBalance(balance-amount);

    }

}
